# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmenuedit
pkgver=5.20.2
pkgrel=0
pkgdesc="KDE menu editor"
# armhf blocked by qt5-qtdeclarative
# s390x and mips64 blocked by polkit (blocked by mozjs, blocked by rust)
arch="all !armhf !s390x !mips64"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0-or-later"
makedepends="extra-cmake-modules qt5-qtbase-dev ki18n-dev kxmlgui-dev kdbusaddons-dev kiconthemes-dev kio-dev kitemviews-dev sonnet-dev kdoctools-dev kglobalaccel-dev kinit-dev"
source="https://download.kde.org/stable/plasma/$pkgver/kmenuedit-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="6ba359c81be841396eca90475a9b9796519f4de01ca281ce5f70d66fb8af16dd97e22dc9960c036b89b3da8942db602f3ca9b9b64d079e81ffc4c683118b9778  kmenuedit-5.20.2.tar.xz"
