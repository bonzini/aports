# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Timo Teräs <timo.teras@iki.fi>
pkgname=youtube-dl
pkgver=2020.11.01.1
pkgrel=0
pkgdesc="Command-line program to download videos from YouTube"
url="https://youtube-dl.org/"
arch="noarch"
license="Unlicense"
depends="python3 py3-setuptools"
checkdepends="py3-flake8 py3-nose"
subpackages="$pkgname-doc
	$pkgname-zsh-completion:zshcomp
	$pkgname-bash-completion
	$pkgname-fish-completion"
source="https://youtube-dl.org/downloads/latest/youtube-dl-$pkgver.tar.gz"
builddir="$srcdir/$pkgname"

prepare() {
	default_prepare
	sed -i \
		-e 's|etc/bash_completion.d|share/bash-completion/completions|' \
		-e 's|etc/fish/completions|share/fish/completions|' \
		"$builddir"/setup.py
}

build() {
	python3 setup.py build
}

check() {
	PYTHON=/usr/bin/python3 make offlinetest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

zshcomp() {
	pkgdesc="Zsh completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel zsh"

	install -Dm644 "$builddir"/$pkgname.zsh \
		"$subpkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="2c84305a71776808797a651e22690a396c1bd4c922ab6f15b086516b836aa6ac3acf5b08f556567b11f470c0d972adbf67fb110dc34ccfc5c0897e576e42ebab  youtube-dl-2020.11.01.1.tar.gz"
