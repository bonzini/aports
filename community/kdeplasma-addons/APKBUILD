# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdeplasma-addons
pkgver=5.20.2
pkgrel=0
pkgdesc="All kind of addons to improve your Plasma experience"
# mips, ppc64le and s390x blocked by qt5-qtwebengine
# armhf blocked by qt5-qtdeclarative
arch="all !ppc64le !s390x !armhf !mips !mips64"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="LGPL-2.0-only AND GPL-2.0-or-later"
depends="purpose"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtwebengine-dev karchive-dev
	kconfig-dev kcoreaddons-dev kdeclarative-dev kholidays-dev ki18n-dev
	kiconthemes-dev kio-dev kcmutils-dev kross-dev knotifications-dev
	plasma-framework-dev krunner-dev kservice-dev sonnet-dev kunitconversion-dev
	kwindowsystem-dev knewstuff-dev"
makedepends="$depends_dev extra-cmake-modules"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/plasma/$pkgver/kdeplasma-addons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# converterrunnertest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "converterrunnertest"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}
sha512sums="be69620657cf6d4bb7642ca65fcf7dc35c8fe72e96a1afe6abbcf68741a9ba4e04539b114106d757c36485e3b32c646c0f559464c2a7d608cc7a030229f25878  kdeplasma-addons-5.20.2.tar.xz"
