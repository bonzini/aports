# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kgamma5
pkgver=5.20.2
pkgrel=0
pkgdesc="Adjust your monitor's gamma settings"
# armhf blocked by extra-cmake-modules
# s390x blocked by kconfigwidgets
arch="all !armhf !s390x"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0-or-later"
makedepends="qt5-qtbase-dev kconfig-dev kconfigwidgets-dev kdoctools-dev ki18n-dev extra-cmake-modules"
source="https://download.kde.org/stable/plasma/$pkgver/kgamma5-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="2f4456fb319979319439f6b50d31b139d8a508a281872c27ac3dfb8bf5c6e54befd254466d2a9c21054d4fb8a46989c1226e74b7b79365bb47ff23e37e31237a  kgamma5-5.20.2.tar.xz"
