# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-sdk
pkgver=5.20.2
pkgrel=0
pkgdesc="Applications useful for Plasma Development"
# armhf blocked by qt5-qtdeclarative
# s390x blocked by kconfigwidgets
# mips64 blocked by multiple dependencies
arch="all !armhf !s390x !mips64"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0-or-later"
depends="kirigami2 qt5-qtquickcontrols"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtsvg-dev karchive-dev kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev kdeclarative-dev ki18n-dev kiconthemes-dev kio-dev plasma-framework-dev kservice-dev ktexteditor-dev kwidgetsaddons-dev kdoctools-dev kparts-dev"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-sdk-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# iconmodeltest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "iconmodeltest"
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="0970ee34587eafc4ec7e463879d97fc8a673aa3139673c678465fc3d5655e87e369dc8f9da1ea845f5c31ac2d04f914def4388d839b59a7f72b181833b7618d7  plasma-sdk-5.20.2.tar.xz"
