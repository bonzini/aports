# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-nm
pkgver=5.20.2
pkgrel=0
pkgdesc="Plasma applet written in QML for managing network connections"
# armhf blocked by qt5-qtdeclarative
# s390x blocked by plasma-framework
arch="all !armhf !s390x"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="(LGPL-2.1-only OR LGPL-3.0-only) AND LGPL-2.0-or-later"
depends="kirigami2 networkmanager"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev ki18n-dev kwindowsystem-dev kservice-dev kcompletion-dev kwidgetsaddons-dev kio-dev kcoreaddons-dev kwallet-dev kconfigwidgets-dev kiconthemes-dev solid-dev kdbusaddons-dev knotifications-dev plasma-framework-dev kdeclarative-dev qca-dev networkmanager-qt-dev modemmanager-qt-dev mobile-broadband-provider-info"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-nm-$pkgver.tar.xz"
subpackages="$pkgname-lang $pkgname-mobile"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_MOBILE=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

mobile() {
	pkgdesc="$pkgdesc (mobile KCM's)"

	mkdir -p \
		"$subpkgdir"/usr/lib/qt5/plugins \
		"$subpkgdir"/usr/share/kservices5

	mv "$pkgdir"/usr/share/kpackage "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/lib/qt5/plugins/kcms "$subpkgdir"/usr/lib/qt5/plugins
	mv \
		"$pkgdir"/usr/share/kservices5/mobilebroadbandsettings.desktop \
		"$pkgdir"/usr/share/kservices5/wifisettings.desktop \
		"$subpkgdir"/usr/share/kservices5/
}
sha512sums="a2120e452ce9c89d18b28a0919a3a45587a57cf4ea664c7e1db5953d6541141950157de91656556c637f8a79537f101244495f0d93402f1afb5f8b00a79e8624  plasma-nm-5.20.2.tar.xz"
