# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-browser-integration
pkgver=5.20.2
pkgrel=0
pkgdesc="Components necessary to integrate browsers into the Plasma Desktop"
# armhf blocked by extra-cmake-modules
# s390x blocked by kio
arch="all !armhf !s390x"
url="https://community.kde.org/Plasma/Browser_Integration"
license="GPL-3.0-or-later"
makedepends="extra-cmake-modules qt5-qtbase-dev kio-dev ki18n-dev kconfig-dev kdbusaddons-dev knotifications-dev krunner-dev kactivities-dev purpose-dev kfilemetadata-dev"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-browser-integration-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="82316e1d7f2892d6b53577b264bc4b158ead7c48b20ffb961a78f4d22304797b3af694ed6128f4658e1205a891f0a7850ad7d5b5f111620aa0dc7d3d98d1b9e7  plasma-browser-integration-5.20.2.tar.xz"
