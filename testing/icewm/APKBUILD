# Contributor: Paul Bredbury <brebs@sent.com>
# Maintainer: Paul Bredbury <brebs@sent.com>
pkgname=icewm
pkgver=1.9.0
pkgrel=0
pkgdesc="Window manager designed for speed, usability and consistency"
url="https://github.com/ice-wm/icewm"
arch="all"
options="!check" # No test suite
license="LGPL-2.0-only"
subpackages="$pkgname-doc $pkgname-lang"
makedepends="alsa-lib-dev
	asciidoctor
	cmake
	fribidi-dev
	gdk-pixbuf-dev
	glib-dev
	libao-dev
	libintl
	libsm-dev
	libsndfile-dev
	libxcomposite-dev
	libxdamage-dev
	libxft-dev
	libxinerama-dev
	libxpm-dev
	libxrandr-dev
	perl"

case "$CARCH" in
	s390x)
		;;
	*)
		makedepends="$makedepends librsvg-dev"
		_arch_opts="-DCONFIG_LIBRSVG=ON"
		;;
esac

source="https://github.com/ice-wm/icewm/releases/download/$pkgver/icewm-$pkgver.tar.lz"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCFGDIR=/etc \
		-DENABLE_NLS=OFF \
		-DENABLE_LTO=ON \
		-DDOCDIR=/usr/share/doc/icewm $_arch_opts
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="781d1ef230fda0b6b52ce6383126e7f953ac043fd85d14de718d42f1271c063d9861fbfd4dd6e81f9b6f33058c59026766ebc3f2fce84ca79c89ddb562c26fde  icewm-1.9.0.tar.lz"
