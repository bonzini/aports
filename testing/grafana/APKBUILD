# Contributor: Konstantin Kulikov <k.kulikov2@gmail.com>
# Maintainer: Konstantin Kulikov <k.kulikov2@gmail.com>
pkgname=grafana
pkgver=7.3.1
pkgrel=0
_commit=6668161a88 # git rev-parse --short HEAD
_stamp=1604052801 # git --no-pager show -s --format=%ct
pkgdesc="Open source, feature rich metrics dashboard and graph editor"
url="https://grafana.com"
arch="all !mips !mips64" # go is missing
license="Apache-2.0"
makedepends="go"
install="$pkgname.pre-install"
subpackages="$pkgname-openrc"
options="net chmod-clean"
source="$pkgname-$pkgver.tar.gz::https://github.com/grafana/grafana/archive/v$pkgver.tar.gz
	$pkgname-$pkgver-bin.tar.gz::https://dl.grafana.com/oss/release/grafana-$pkgver.linux-amd64.tar.gz
	$pkgname.initd
	$pkgname.confd"

export GOPATH=${GOPATH:-$srcdir/go}
export GOCACHE=${GOCACHE:-$srcdir/go-build}
export GOTMPDIR=${GOTMPDIR:-$srcdir}

# secfixes:
#   7.0.2-r0:
#     - CVE-2020-13379
#   6.3.4-r0:
#     - CVE-2019-15043

build() {
	local ldflags="-X main.version=$pkgver -X main.commit=$_commit -X main.buildstamp=$_stamp"
	go build -ldflags "$ldflags" -v github.com/grafana/grafana/pkg/cmd/grafana-server
	go build -ldflags "$ldflags" -v github.com/grafana/grafana/pkg/cmd/grafana-cli
}

check() {
	local pkgs="./..."
	case "$CARCH" in
	# https://github.com/grafana/grafana/issues/26389
	x86) pkgs="$(go list ./... | grep -Ev '(pkg/tsdb$)')" ;;
	# https://github.com/grafana/grafana/issues/26390
	s390x) pkgs="$(go list ./... | grep -Ev '(pkg/tsdb/influxdb/flux$)')" ;;
	esac

	go test $pkgs
}

package() {
	install -Dm755 "$srcdir/$pkgname.initd" "$pkgdir/etc/init.d/$pkgname"
	install -Dm644 "$srcdir/$pkgname.confd" "$pkgdir/etc/conf.d/$pkgname"
	install -Dm755 "$builddir/$pkgname-server" "$pkgdir/usr/sbin/$pkgname-server"
	install -Dm755 "$builddir/$pkgname-cli" "$pkgdir/usr/bin/$pkgname-cli"
	install -Dm644 "$builddir/conf/sample.ini" "$pkgdir/etc/grafana.ini"
	install -dm755 "$pkgdir/usr/share/grafana"
	cp -r "$builddir/conf" "$builddir/public" "$pkgdir/usr/share/$pkgname/"
}

sha512sums="95e12158c58f99e5479aa042eccaed402957f3d05b8ecaf36aed8737ad3a33970a7204aeb1e6da1e58beb3144f29acf98ffa73b96fd6469ce73e66be0ac85f36  grafana-7.3.1.tar.gz
0436b5bec58e52582542bb1efe7825ffc7abd5289d82916c6a54c6aee77a7350060ef877734b32332a5f3935cf1a3404b670e5f9344a8bb9b5c91d95ad81e214  grafana-7.3.1-bin.tar.gz
b0a781e1b1e33741a97e231c761b1200239c6f1235ffbe82311fe883387eb23bef262ad68256ebd6cf87d74298041b53b947ea7a493cfa5aa814b2a1c5181e13  grafana.initd
c2d9896ae9a9425f759a47aeab42b7c43b63328e82670d50185de8c08cda7b8df264c8b105c5c3138b90dd46e86598b16826457eb3b2979a899b3a508cbe4e8c  grafana.confd"
